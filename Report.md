# Milestone 1: Planning Report


## Overview

Our web application will use Facebook API to search public profiles on Facebook, and return listed search results of matching profiles. It will then enable you to select a profile and view more detailed information, that this user has made available.

Initial setup stages involved, communications via Slack and collaborating together in class tutorials to establish the team leader, what API we would use and how we would implement this to create our project application. Once defined, we also configured our project management tool (Trello), and a Bitbucket repository to centralise our work flow and keep track of tasks. 

Further discussions revealed the user flow and from this we determined what each page objective is within the process. This naturally componentise our project in to three main parts, where we have assigned these tasks amongst us, and have tried to balance responsibilities as equal as possible. We will create a wireframe for each page based on its function i.e. search, results, profile, and also use a three tier approach catering to major devices i.e. mobile, tablet and computer.

We have used the resources listed in the section below, to help reference material that would enable us to complete this project successfully. The tools section would also detail the software and services that we use to coordinate and manage our resources, develop, code, test and communicate with one another.

As we found later on, we also realised that we need a separate help page which played into our favour, as it allowed us to define a menu link which we were missing, and also that it is a requirement.

## Application Flow

#### Home Page
The home page will display the search bar as the main focal point, where you will enter a name in the search, and it will then send you to the results page to be processed.

#### Results Page 
From there the results page will then return any results from the Facebook API and list these on the page, where a user can then select a profile to view.

#### Profile Page
Once selected on the results page, it will take you to this page where this will display the public info of the chosen profile chose, also have a back button to take you back to the results page.


## Team Members and Roles

#### Nigel Walters
* Team leader
* Results page
    * Receive search query, from home page (GET).
    * Query Facebook API.
    * List any returned profile/s.

#### Cameron Coyle
* Home page
    * Initial search (no results), only search bar as main input focus.
    * Send search query to results page.
* Help page
    * List detailed information on how to use the site

#### Jaskarnjit Singh Dhami
* Profile page
    * Get profile from results page.
    * List available profile details.
* Issue tracking

#### Group Tasks
While doing our assigned tasks, we will also help each other once they have been done or if we are awaiting completion of tasks that are required for us to proceed further. We will also continually review and test our code for any issues/bugs, and log these to be fixed.

* Setup website backend
    * Views
    * Controllers
    * Static content e.g. JavaScript, CSS, libraries etc.
* Responsive layout, each team member responsible for their own pages.


## Resources

#### MVC Core
* <https://www.youtube.com/playlist?list=PLs5n5nYB22fIqNHp8kHP6dLW5DZYJnWxz>
* <https://docs.microsoft.com/en-us/aspnet/core/mvc/overview>

#### Facebook
* <https://www.youtube.com/watch?v=gXux8b3wcYw>
* <https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r>
* <https://developers.facebook.com/>

#### JavaScript
* <https://www.youtube.com/watch?v=m14hYs1T3FA&list=PLYPFxrXyK0BwiXNe09hTPjFqYbsWv8gxb>
* <https://jquery.com/>

#### Web Development
* <https://www.w3schools.com/>

#### Bootstrap
* <http://getbootstrap.com/>

#### Troubleshooting
* <https://www.google.co.nz/>
* <https://stackoverflow.com/>


## Wireframes

Our wireframes will consist of the following pages:

* Home  
* Results
* Profile
* Help

Jaz drew up an initial mock flow process based on the main pages that our site would consist of. This was to give us a better idea of what we would be dealing with, and how we would look to proceed further:

[Inital wireframe - draw.io](https://drive.google.com/a/bcs.net.nz/file/d/0B6hF7Ow9ZLtZcnh4UjBsWWpQNTg/view?usp=sharing)

It was decided to cater for three layout sizes as follows, in which they do not distinguish orientation:

* Mobile
* Tablet
* Desktop

We created a rough wireframe sketch on what we thought we would need for the pages on our site, for each layout type, and used this as a guideline to produce our final wireframe:

[Wireframe sketch.pdf](https://drive.google.com/a/bcs.net.nz/file/d/0B2Ge8lY2w_seVTJmOTlLSHRFUmc/view?usp=sharing)

The final wireframe was in large based upon the sketch, but as things eventuated some aspects needed to be included like the heading for the name of a page, and also a menu for the additional help page that we did not anticipate for. Another thought is to possibly have a back button on the profile page to go back to the results page (as stated in the Application Flow section), to make it easier for the user to do so. At the moment there is the search bar instead so that a user may search again without completely reverting back. As we may hope that a user could use the back page button of the browser itself to return to the results page if necessary, but we should not make this complete assumption. Possibly could implement both but don't want to clutter the page with too many options, we will see when we develop/test the application (work in progress).

[Final wireframe - draw io](https://drive.google.com/a/bcs.net.nz/file/d/0B2Ge8lY2w_seR3Zidms2V2E3cUk/view?usp=sharing)

## Tools

[Slack](https://slack.com/)  
Primary virtual communiction channel used to communicate out of class, share code and resources.

[Trello](https://trello.com/)  
To help plan and organise tasks throughout this project, its easy to set up we'll have 3 tabs 'To do', 'Doing', and 'Done'. These tasks will be moved to where they belong so everyone can access them to add or edit.

[Bitbucket](https://bitbucket.org/)  
Centralised Git repository, used to collate, manage and collaborate resources for our team project. 

[Draw.io](https://www.draw.io/)

[Visual Studio](https://www.visualstudio.com/)  
Using the differnt flavours that this suite offers. Visual Studio Enterprise 2017 for its vast intellisense coding in various languages and its great support for C# debugging and Visual Studio Code for coding also and using it as a markdown editor.

[Git Bash (for Windows)](https://git-for-windows.github.io/)  
Issuing Git commands through the Git Bash command line interface (CLI), to (push) and from (pull) our repository.

[Browsers](https://browsehappy.com/)  
Predominantly using the major browsers to test the responsive layout of our site, and to ensure consistency across these browser platforms. Also taking advantage of their frontend debugging facitlities especially for JavaScript but also HTML and CSS too.
