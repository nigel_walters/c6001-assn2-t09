function searchFacebook (query){
    console.log(query);
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          // the user is logged in and has authenticated your
          // app, and response.authResponse supplies
          // the user's ID, a valid access token, a signed
          // request, and the time the access token 
          // and signed request each expire
          var accessToken = response.authResponse.accessToken;
          FB.api("search?type=user&q="+query+"&fields=id,name,picture.width(200).height(200)&access_token="+accessToken, function(response){
            if(!response || response.error){
                console.log('error');
            }
            else{
                showResults(response);
            }
        });
          
        } else if (response.status === 'not_authorized') {
          // not authenticated your app
          console.log('unauthorised');
          alert('Please authorise this app to access the required permissions');
        } else {
            console.log('not logged in');
            alert('Please log in to facebook');
          // the user isn't logged in to Facebook.
        }
       });
}

function showResults(response)
{
    console.log(response);
    if(response.data.length > 0){
    $('#results').empty();

    for(var i = 0; i < response.data.length; i++){
        $('#status').text('');
        var html = `
        <div class="col-sm-12 col-lg-6 list-result">
            <a href="profile/${response.data[i].id}">
                <div class="col-sm-3 profile-image">
                    <img class="img-rounded" src="${response.data[i].picture.data.url}" />
                </div>
                <div class="col-sm-9 profile-name">
                   ${response.data[i].name}
                </div>
            </a>
        </div>`;
        $('#results').append(html);

    }
}
else{
    $('#status').text("No results");
}
}