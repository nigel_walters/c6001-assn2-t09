function getProfile(id) {
    console.log(id);
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          var accessToken = response.authResponse.accessToken;
          FB.api(id+"?fields=id,cover,name,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified&access_token="+accessToken, function(response){
            if(!response || response.error){
                console.log('error');
            }
            else{
              
                profile(response)
            }
        });
          
        } else if (response.status === 'not_authorized') {
          console.log('unauthorised');
          alert('Please authorise this app to access the required permissions');
        } else {
            console.log('not logged in');
            alert('Please log in to facebook');
        }
       });
}



function profile(response){
    console.log(response);
        let profile = `
        <div class="col-xs-12 col-md-6">
        <img class="img-rounded img-responsive" src=" ${response.cover.source}" /><br>
        <div>
            <img class="img-rounded" src="${response.picture.data.url}" />
            <a href="${response.link}" style="text-decoration: none; padding-left: 10px"><span class="profile-name" style="vertical-align: bottom;"> ${response.name}</span></a>
            </div>
        </div>
            <div class="col-xs-12 col-md-6">
            <ul class="list-group" id="profile-list">
                <li class="list-group-item">User ID: ${response.id}</li>
                <li class="list-group-item">First Name: ${response.first_name}</li>
                <li class="list-group-item">First Name: ${response.last_name}</li>
                <li class="list-group-item">Age Range: ${response.age_range}</li>
                <li class="list-group-item">Link: <a href="${response.link}">${response.link}</a></li>
                <li class="list-group-item">Gender: ${response.gender}</li>
                <li class="list-group-item">Locale: ${response.locale}</li>
                <li class="list-group-item">Timezone: ${response.timezone}</li>
                <li class="list-group-item">Updated Time: ${response.updated_time}</li>            
                <li class="list-group-item">Verified: ${response.verified}</li>
            </ul>
            </div>
        `;
        document.getElementById('profile').innerHTML = profile;
      }